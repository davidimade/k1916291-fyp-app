package com.example.fypapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.QuestionVH> {

    List<Questions> questionsList;

    public QuestionsAdapter(List<Questions> questionsList) {
        this.questionsList = questionsList;
    }

    @NonNull
    @Override
    public QuestionVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
             View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
             return new QuestionVH(view);

    }

    @Override
    public void onBindViewHolder(@NonNull QuestionVH holder, int position) {

        Questions questions = questionsList.get(position);
        holder.questionsTxt.setText(questions.getQuestion());
        holder.descriptionTxt.setText(questions.getDescription());

        boolean isExpandable = questionsList.get(position).isExpandable();
        holder.expandableLayout.setVisibility(isExpandable ? View.VISIBLE : View.GONE);


    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    public class QuestionVH extends RecyclerView.ViewHolder {

        TextView questionsTxt, descriptionTxt;
        LinearLayout linearLayout;
        RelativeLayout expandableLayout;

        public QuestionVH(@NonNull View itemView) {
            super(itemView);

            questionsTxt = itemView.findViewById(R.id.question);
            descriptionTxt = itemView.findViewById(R.id.description);

            linearLayout = itemView.findViewById(R.id.linear_layout);
            expandableLayout = itemView.findViewById(R.id.expandable_layout);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Questions questions = questionsList.get(getAdapterPosition());
                    questions.setExpandable(!questions.isExpandable());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }
}
