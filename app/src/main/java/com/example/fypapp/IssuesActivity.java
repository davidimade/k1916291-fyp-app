package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.SearchView;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class IssuesActivity extends AppCompatActivity {

    public static ArrayList<Issue> issueList = new ArrayList<Issue>();

    private ListView listView;
    private Button accidentButton;
    private Button amendedServiceButton;
    private Button busButton;
    private Button constructionButton;
    private Button resetButton;

    private String selectedFilter = "reset";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.status);

        bottomNavigationView.setOnItemSelectedListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.status:
                            startActivity(new Intent(getApplicationContext(), StatusActivity.class));
                            finish();
                            return true;
                        case R.id.issues:
                            return true;
                        case R.id.maps:
                            startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                            return true;
                        case R.id.more:
                            startActivity(new Intent(getApplicationContext(), MoreActivity.class));
                    }
                    return false;

                });

        setUpData();
        setUpList();
        setOnClickListener();
        initSearchWidgets();
        initFilterWidgets();
    }


    private void initFilterWidgets() {

        Button accidentButton = (Button) findViewById(R.id.accidentFilter);
        Button amendedServiceButton = (Button) findViewById(R.id.amendedServiceFilter);
        Button busButton = (Button) findViewById(R.id.busReplacementFilter);
        Button constructionButton = (Button) findViewById(R.id.constructionFilter);
        Button resetButton = (Button) findViewById(R.id.resetFilter);


    }
    private void setUpData() {
        Issue accident = new Issue("0", "Accident at Mitcham Eastfields Station. Services may or may not be running until 22nd May.", R.drawable.accident_icon);
        issueList.add(accident);

        Issue amendedServices = new Issue("1", "Amended Services at Clapham Junction until the 23rd of June", R.drawable.amended_services);
        issueList.add(amendedServices);

        Issue busReplacement = new Issue("2", "Bus replacement at Wimbledon Station until 14th July.", R.drawable.bus_replacement);
        issueList.add(busReplacement);

        Issue construction = new Issue("3", "Construction at Kingston Station until further notice.", R.drawable.construction);
        issueList.add(construction);
}

    private void setUpList() {
        listView = (ListView) findViewById(R.id.issuesListView);

        IssuesAdapter adapter = new IssuesAdapter(getApplicationContext(), 0, issueList);
        listView.setAdapter(adapter);

    }

    private void setOnClickListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Issue selectIssue = (Issue) (listView.getItemAtPosition(position));
                Intent showDetail = new Intent(getApplicationContext(), DetailActivity.class);
                showDetail.putExtra("id", selectIssue.getId());
                startActivity(showDetail);
            }
        });
    }
        private void initSearchWidgets()
        {
            SearchView searchView = (SearchView) findViewById(R.id.issuesSearchView);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String s)
                {
                    ArrayList<Issue> filteredIssue = new ArrayList<Issue>();

                    for(Issue issue: issueList)
                    {
                        if(issue.getName().toLowerCase().contains(s.toLowerCase()))
                        {
                            filteredIssue.add(issue);
                        }
                    }

                    IssuesAdapter adapter = new IssuesAdapter(getApplicationContext(),0, filteredIssue);
                    listView.setAdapter(adapter);

                    return false;
                }
            });
        }


        public void filterList(String status)
        {
            selectedFilter = status;
            ArrayList<Issue> filteredIssue = new ArrayList<Issue>();
            for(Issue issue: issueList)
            {
                if(issue.getName().toLowerCase().contains(status))
                {
                    filteredIssue.add(issue);
                }
            }
            IssuesAdapter adapter = new IssuesAdapter(getApplicationContext(), 0, filteredIssue);
            listView.setAdapter(adapter);
        }


        public void resetFilterTapped(View view)
    {
        selectedFilter = "reset";
        IssuesAdapter adapter = new IssuesAdapter(getApplicationContext(), 0, issueList);
        listView.setAdapter(adapter);
    }

        public void accidentFilterTapped(View view) {
          filterList("Accident");
        }

    public void amendedFilterTapped(View view) {
          filterList("Amended Service");
    }

    public void busFilterTapped(View view) {
          filterList("Bus Replacement");
    }

    public void constructionFilterTapped(View view) {
          filterList("Construction");
    }

}