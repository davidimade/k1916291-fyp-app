package com.example.fypapp;

public class Issue {

    private String id;
    private String name;
    private int Image;

    public Issue(String id, String name, int image) {
        this.id = id;
        this.name = name;
        Image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }
}
