package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MoreActivity extends AppCompatActivity {

    Button settings_Button, about_UsButton, FAQ_Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        settings_Button = findViewById(R.id.settingsButton);
        about_UsButton = findViewById(R.id.aboutUsButton);
        FAQ_Button = findViewById(R.id.FAQButton);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.status);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.status:
                    startActivity(new Intent(getApplicationContext(), StatusActivity.class));
                    finish();
                    return true;
                case R.id.issues:
                    startActivity(new Intent(getApplicationContext(), IssuesActivity.class));
                    finish();
                    return true;
                case R.id.maps:
                    startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                    return true;
                case R.id.more:
            }
            return false;

        });
        settings_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 startActivity(new Intent(MoreActivity.this, SettingsActivity.class));
            }
        });
        about_UsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MoreActivity.this, AboutUsActivity.class));
            }
        });
        FAQ_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MoreActivity.this, FAQActivity.class));
            }
        });
    }
}