package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class FAQActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Questions> questionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.status);

        bottomNavigationView.setOnItemSelectedListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.status:
                            startActivity(new Intent(getApplicationContext(), StatusActivity.class));
                            finish();
                            return true;
                        case R.id.issues:
                            startActivity(new Intent(getApplicationContext(), IssuesActivity.class));
                            finish();
                            return true;
                        case R.id.maps:
                            startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                            return true;
                        case R.id.more:
                    }
                    return false;

                });
     recyclerView = findViewById(R.id.recyclerView);

        initData();
        setRecyclerView();


    }

    private void setRecyclerView() {
        QuestionsAdapter questionsAdapter = new QuestionsAdapter(questionsList);
        recyclerView.setAdapter(questionsAdapter);
        recyclerView.setHasFixedSize(true);
    }

    private void initData() {
        questionsList = new ArrayList<>();

        questionsList.add(new Questions("What do each four of the status services mean?"," Good services represent no current issues with the train service.\n" +
                "\n" +
                "Minor Delays represent a small number of delays or small issues that may slightly affect travel.\n" +
                "\n" +
                "Severe Delays represents that there is a serious ongoing issue with the train service and many trains may be subjected to long delays or cancellations.\n" +
                "\n" +
                "Shut Down represents that the train service is currently not in use. No trains will be running on these services."));
        questionsList.add(new Questions("What do the issue types mean?","The first type of Issue is Accident.\n" +
                "The second is Amended Services.\n" +
                "The third is Bus Replacement.\n" +
                "The fourth is Construction. "));
    }

}