package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    Issue selectedIssue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSelectedIssue();
        setValues();
    }

    private void getSelectedIssue()
    {
        Intent previousIntent = getIntent();
        String parsedStringID = previousIntent.getStringExtra("id");
        selectedIssue = IssuesActivity.issueList.get(Integer.valueOf(parsedStringID));
    }

    private void setValues() {

        TextView tv = (TextView) findViewById(R.id.issueName);
        ImageView iv = (ImageView) findViewById(R.id.issueImage);

        tv.setText(selectedIssue.getName());
        iv.setImageResource(selectedIssue.getImage());

    }
}