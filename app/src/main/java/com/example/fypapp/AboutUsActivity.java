package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.status);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.status:
                    startActivity(new Intent(getApplicationContext(), StatusActivity.class));
                    finish();
                    return true;
                case R.id.issues:
                    startActivity(new Intent(getApplicationContext(), IssuesActivity.class));
                    return true;
                case R.id.maps:
                    startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                    return true;
                case R.id.more:

            }
            return false;

        });

        ;
    }
    }
