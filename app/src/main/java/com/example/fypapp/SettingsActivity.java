package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class SettingsActivity extends AppCompatActivity {

    NotificationManagerCompat notificationManagerCompat;
    Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("noti","Notification Name", NotificationManager.IMPORTANCE_DEFAULT);

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "noti")
                .setSmallIcon(R.drawable.train_service_logo)
                .setContentTitle("Issue: Amended Services")
                .setContentText("Amended Services at Clapham Junction \n until the 23rd of June");

        notification = builder.build();

        notificationManagerCompat = NotificationManagerCompat.from(this);
    }

    public void push(View view) {
        notificationManagerCompat.notify(1, notification);
    }
}
