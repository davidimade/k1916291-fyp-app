package com.example.fypapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class StatusAdapter extends ArrayAdapter<Status> {

    public StatusAdapter(Context context, int resource, List<Status> statusList)
    {
        super(context, resource, statusList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Status status = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.status_cell, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.statusName);
        ImageView iv = (ImageView) convertView.findViewById(R.id.statusImage);

        tv.setText(status.getName());
        iv.setImageResource(status.getImage());


        return convertView;
    }}
