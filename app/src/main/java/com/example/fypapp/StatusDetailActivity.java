package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class StatusDetailActivity extends AppCompatActivity {

    Status selectedStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statusdetail);
        getSelectedIssue();
        setValues();
    }

    private void getSelectedIssue()
    {
        Intent previousIntent = getIntent();
        String parsedStringID = previousIntent.getStringExtra("id");
        selectedStatus = StatusActivity.statusList.get(Integer.valueOf(parsedStringID));
    }

    private void setValues() {

        TextView tv = (TextView) findViewById(R.id.statusName);
        ImageView iv = (ImageView) findViewById(R.id.statusImage);

        tv.setText(selectedStatus.getName());
        iv.setImageResource(selectedStatus.getImage());

    }
}