package com.example.fypapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class IssuesAdapter extends ArrayAdapter<Issue> {

    public IssuesAdapter(Context context, int resource, List<Issue> issueList)
    {
        super(context, resource, issueList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Issue issue = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.issue_cell, parent, false);
    }
      TextView tv = (TextView) convertView.findViewById(R.id.issueName);
      ImageView iv = (ImageView) convertView.findViewById(R.id.issueImage);

      tv.setText(issue.getName());
      iv.setImageResource(issue.getImage());


        return convertView;
}}
