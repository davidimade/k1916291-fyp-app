package com.example.fypapp;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.SearchView;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.fypapp.databinding.ActivityMainBinding;
import com.example.fypapp.databinding.ActivityStatusBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StatusActivity extends AppCompatActivity {

    public static ArrayList<Status> statusList = new ArrayList<Status>();

    private ListView listView;
    private Button accidentButton;
    private Button amendedServiceButton;
    private Button busButton;
    private Button constructionButton;
    private Button resetButton;

    private String selectedFilter = "reset";

    ActivityStatusBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        binding = ActivityStatusBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setUpData();
        setUpList();
        setOnClickListener();
        initSearchWidgets();
        initFilterWidgets();


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.status);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.status:
                    return true;
                case R.id.issues:
                    startActivity(new Intent(getApplicationContext(), IssuesActivity.class));
                    finish();
                    return true;
                case R.id.maps:
                    startActivity(new Intent(getApplicationContext(), GoogleMapsActivity.class));
                    return true;
                case R.id.more:
                    startActivity(new Intent(getApplicationContext(), MoreActivity.class));
            }
            return false;

        });

        ;
    }
    private void initFilterWidgets() {

        Button accidentButton = (Button) findViewById(R.id.accidentFilter);
        Button amendedServiceButton = (Button) findViewById(R.id.amendedServiceFilter);
        Button busButton = (Button) findViewById(R.id.busReplacementFilter);
        Button constructionButton = (Button) findViewById(R.id.constructionFilter);
        Button resetButton = (Button) findViewById(R.id.resetFilter);


    }
    private void setUpData() {
        Status goodService = new Status("0", "Great Western Railway \n Good Service \n No Issues With Service", R.drawable.great_western_railway);
        statusList.add(goodService);

        Status minorDelays = new Status("1", "Southern Rail \n Minor Delays,\n Problem with signals at London Blackfriars", R.drawable.southern_rail);
        statusList.add(minorDelays);

        Status severeDelays = new Status("2", "Northern Rail \n Severe Delays, \n Flooding at Castleford", R.drawable.northern_rail);
        statusList.add(severeDelays);

        Status shutDown = new Status("3", "Southeastern \n Shut Down, \n Severe accident at West Dulwich. No trains until further notice", R.drawable.southeastern);
        statusList.add(shutDown);

        Status goodService2 = new Status("4", "ThamesLink \n Good Service \n No Issues With Service", R.drawable.thameslink);
        statusList.add(goodService2);

        Status goodService3 = new Status("5", "West Midlands Trains \n Good Service \n No Issues With Service", R.drawable.west_midlands);
        statusList.add(goodService3);

        Status goodService4 = new Status("6", "Great Northern \n Good Service \n No Issues With Service", R.drawable.great_northern);
        statusList.add(goodService4);

        Status goodService5 = new Status("7", "London North Eastern Railway \n Good Service \n No Issues With Service", R.drawable.london_north_eastern_railway);
        statusList.add(goodService5);
    }

    private void setUpList() {
        listView = (ListView) findViewById(R.id.statusListView);

        StatusAdapter adapter = new StatusAdapter(getApplicationContext(), 0, statusList);
        listView.setAdapter(adapter);

    }

    private void setOnClickListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Status selectStatus = (Status) (listView.getItemAtPosition(position));
                Intent showStatusDetail = new Intent(getApplicationContext(), StatusDetailActivity.class);
                showStatusDetail.putExtra("id", selectStatus.getId());
                startActivity(showStatusDetail);
            }
        });
    }
    private void initSearchWidgets()
    {
        SearchView searchView = (SearchView) findViewById(R.id.statusSearchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s)
            {
                ArrayList<Status> filteredStatus = new ArrayList<Status>();

                for(Status status: statusList)
                {
                    if(status.getName().toLowerCase().contains(s.toLowerCase()))
                    {
                        filteredStatus.add(status);
                    }
                }

                StatusAdapter adapter = new StatusAdapter(getApplicationContext(),0, filteredStatus);
                listView.setAdapter(adapter);

                return false;
            }
        });
    }


    public void filterList(String status2)
    {
        selectedFilter = status2;
        ArrayList<Status> filteredStatus = new ArrayList<Status>();
        for(Status status: statusList)
        {
            if(status.getName().toLowerCase().contains(status2))
            {
                filteredStatus.add(status);
            }
        }
        StatusAdapter adapter = new StatusAdapter(getApplicationContext(), 0, filteredStatus);
        listView.setAdapter(adapter);
    }


    public void resetFilterTapped(View view)
    {
        selectedFilter = "reset";
        StatusAdapter adapter = new StatusAdapter(getApplicationContext(), 0, statusList);
        listView.setAdapter(adapter);
    }

    public void accidentFilterTapped(View view) {
        filterList("Good Service");
    }

    public void amendedFilterTapped(View view) {
        filterList("Minor Delays");
    }

    public void busFilterTapped(View view) {
        filterList("Severe Delays");
    }

    public void constructionFilterTapped(View view) {
        filterList("Shut Down");
    }





    }


